/*******************************************************************************

    Copyright(C) Jonas 'Sortie' Termansen 2011, 2012.

    This file is part of Sortix.

    Sortix is free software: you can redistribute it and/or modify it under the
    terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.

    Sortix is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
    details.

    You should have received a copy of the GNU General Public License along with
    Sortix. If not, see <http://www.gnu.org/licenses/>.

    sortix/kernel/kernel.h
    Base header for the Sortix kernel that includes common stuff.

*******************************************************************************/

#ifndef SORTIX_KERNEL_H
#define SORTIX_KERNEL_H

#include <sortix/kernel/decl.h>
#include <sortix/kernel/log.h>
#include <sortix/kernel/panic.h>

#endif
